<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'telefone',
        'email',
        'aceita_sms',
        'aceita_email',
        'celular',
        'rg',
        'cpf',
        'parentesco',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
