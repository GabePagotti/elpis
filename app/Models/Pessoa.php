<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    use HasFactory;

    protected $fillable = [
        'face_id',
        'foto',
        'nome',
        'dataNascimento',
        'rg',
        'cpf',
        'corPele',
        'corOlho',
        'corCabelo',
        'altura',
        'vestimentas',
        'sexo',
        'isDesaparecido',
    ];

    protected $casts = [
        'dataNascimento' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function locais()
    {
        return $this->hasMany(Local::class);
    }
}
