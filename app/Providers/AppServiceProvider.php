<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('admin', function () {
            return Auth::user()->permissao == 'admin';
        });

        Blade::directive('cpf', function ($expression) {
            return "<?php echo substr($expression,0,3).'.'.substr($expression,3,3).'.'.substr($expression,6,3).'-'.substr($expression,-2); ?>";
        });

        Blade::directive('cep', function ($expression) {
            return "<?php echo substr($expression,0,5).'-'.substr($expression,-3); ?>";
        });

        Blade::directive('rg', function ($expression) {
            return "<?php echo substr($expression,0,2).'.'.substr($expression,2,3).'.'.substr($expression,5,3).'-'.substr($expression,-1); ?>";
        });

        Blade::directive('idade', function ($expression) {
            return "<?php echo {$expression}->diff(\Carbon\Carbon::now())->format('%y anos'); ?>";
        });

        Blade::directive('telefone', function ($expression) {
            return "<?php echo '('.substr($expression,0,2).') '.substr($expression,2,4).'-'.substr($expression,6,10); ?>";
        });

        Blade::directive('celular', function ($expression) {
            return "<?php echo '('.substr($expression,0,2).') '.substr($expression,2,5).'-'.substr($expression,7,11); ?>";
        });
    }
}
