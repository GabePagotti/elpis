<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $userRepository;

    function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $this->authorize('viewAny', User::class);
        $users = $this->userRepository->all();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $this->authorize('create', User::class);

        return view('users.form');
    }

    public function store(UserRequest $request)
    {
        $this->authorize('create', User::class);

        $user = $this->userRepository->store($request);

        return redirect()->route('users.show', $user->id);
    }

    public function show($id)
    {
        $user = $this->userRepository->find($id);
        $this->authorize('view', $user);

        return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        $this->authorize('update', $user);

        return view('users.form', compact('user'));
    }

    public function update($id, UserRequest $request)
    {
        $user = $this->userRepository->find($id);
        $this->authorize('update', $user);

        $this->userRepository->update($user, $request);

        return redirect()->route('users.show', $user->id);
    }

    public function destroy($id)
    {
        $user = $this->userRepository->find($id);
        $this->authorize('delete', $user);

        $this->userRepository->destroy($user);

        return redirect()->route('users.index');
    }
}
