<?php

namespace App\Http\Controllers;

use App\Http\Requests\PessoaRequest;
use App\Models\Pessoa;
use App\Repositories\PessoaRepository;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Http\Request;

class PessoaController extends Controller
{
    protected $pessoaRepository;

    function __construct(PessoaRepository $pessoaRepository)
    {
        $this->pessoaRepository = $pessoaRepository;
    }

    public function index()
    {
        $this->authorize('viewAny', Pessoa::class);
        $pessoas = $this->pessoaRepository->all();

        return view('pessoas.index', compact('pessoas'));
    }

    public function create()
    {
        $this->authorize('create', Pessoa::class);

        return view('pessoas.form');
    }

    public function store(PessoaRequest $request)
    {
        $this->authorize('create', Pessoa::class);

        $pessoa = $this->pessoaRepository->store($request);

        return redirect()->route('pessoas.show', $pessoa->id);
    }

    public function show($id)
    {
        $pessoa = $this->pessoaRepository->find($id);
        $this->authorize('view', $pessoa);

        return view('pessoas.show', compact('pessoa'));
    }

    public function edit($id)
    {
        $pessoa = $this->pessoaRepository->find($id);
        $this->authorize('update', $pessoa);

        return view('pessoas.form', compact('pessoa'));
    }

    public function update($id, PessoaRequest $request)
    {
        $pessoa = $this->pessoaRepository->find($id);
        $this->authorize('update', $pessoa);

        $this->pessoaRepository->update($pessoa, $request);

        return redirect()->route('pessoas.show', $pessoa->id);
    }

    public function destroy($id)
    {
        $pessoa = $this->pessoaRepository->find($id);
        $this->authorize('delete', $pessoa);

        $this->pessoaRepository->destroy($pessoa);

        return redirect()->route('pessoas.index');
    }

    public function buscar(Request $request)
    {
        $results = $this->pessoaRepository->buscarByImage($request->foto);

        $pessoas = $this->pessoaRepository->getByFaceIds($results);

        return view('pessoas.results', compact('pessoas'));
    }
}
