<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PessoaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foto' => ['required_without:id', 'image',],
            'nome' => ['required', 'string', 'between:5,255'],
            'dataNascimento' => ['required', 'date_format:Y-m-d',],
            'rg' => ['required', 'string', 'size:9',],
            'cpf' => ['required', 'string', 'size:11',],
            'corPele' => ['required', 'string', 'between:5,255',],
            'corOlho' => ['required', 'string', 'between:5,255',],
            'corCabelo' => ['required', 'string', 'between:5,255',],
            'altura' => ['required', 'integer',],
            'vestimentas' => ['required', 'string', 'between:5,255',],
            'sexo' => ['required', 'string', 'in:F,M',],
            'desaparecido' => ['nullable', 'in:1',],
        ];
    }
}
