<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => ['required', 'string', 'max:255'],
            'telefone' => ['required', 'string', 'size:10'],
            'email' => ['required', 'string', 'email', 'max:255',],
            'aceita_sms' => ['in:1', 'nullable'],
            'aceita_email' => ['in:1', 'nullable'],
            'celular' => ['required', 'size:11'],
            'rg' => ['required', 'string', 'size:9',],
            'cpf' => ['required', 'string', 'size:11',],
            'parentesco' => ['required', 'string', 'max:255'],
        ];
    }
}
