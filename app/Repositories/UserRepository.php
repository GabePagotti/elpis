<?php
namespace App\Repositories;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function find($id)
    {
        return User::findOrFail($id);
    }

    public function all()
    {
        return User::all();
    }

    public function store(UserRequest $request)
    {
        return User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'permissao' => $request->input('permissao', 'user'),
            'password' => Hash::make($request->input('password')),
        ]);
    }

    public function update(User $user, UserRequest $request)
    {
        $user->fill([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'permissao' => $request->input('permissao', 'user'),
        ]);

        $senha = $request->input('password');
        if ($senha) {
            $user->password = Hash::make($senha);
        }

        $user->save();
    }

    public function destroy(User $user)
    {
        $user->delete();
    }
}