<?php
namespace App\Repositories;

use App\Http\Requests\PessoaRequest;
use App\Models\Pessoa;
use Auth;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class PessoaRepository
{
    protected function getAWSClient()
    {
        return new RekognitionClient([
            'region'    => 'us-west-2',
            'version'   => 'latest'
        ]);
    }

    public function find($id)
    {
        return Pessoa::findOrFail($id);
    }

    public function all()
    {
        return Auth::user()->pessoas;
    }

    public function store(PessoaRequest $request)
    {
        $user = Auth::user();

        $foto = $request->file('foto');
        $caminho = $foto->storeAs('fotos', uniqid().'.'.$foto->extension());

        $faceId = $this->addFaceToCollection($caminho);

        return $user->pessoas()->create([
            'face_id' => $faceId,
            'foto' => $caminho,
            'nome' => $request->input('nome'),
            'dataNascimento' => $request->input('dataNascimento'),
            'rg' => $request->input('rg'),
            'cpf' => $request->input('cpf'),
            'corPele' => $request->input('corPele'),
            'corOlho' => $request->input('corOlho'),
            'corCabelo' => $request->input('corCabelo'),
            'altura' => $request->input('altura'),
            'vestimentas' => $request->input('vestimentas'),
            'sexo' => $request->input('sexo'),
            'isDesaparecido' => $request->input('desaparecido') ? 1 : 0,
        ]);
    }

    protected function addFaceToCollection($caminho)
    {
        $client = $this->getAWSClient();

        $image = fopen($caminho, 'r');

        $bytes = fread($image, filesize($caminho));

        $result = $client->IndexFaces([
            'CollectionId' => 'AriadneFaces',
            'Image' => ['Bytes' => $bytes],
        ]);

        return $result->get('FaceRecords')[0]['Face']['FaceId'];
    }

    public function update(Pessoa $pessoa, PessoaRequest $request)
    {
        $pessoa->fill([
            'nome' => $request->input('nome'),
            'dataNascimento' => $request->input('dataNascimento'),
            'rg' => $request->input('rg'),
            'cpf' => $request->input('cpf'),
            'corPele' => $request->input('corPele'),
            'corOlho' => $request->input('corOlho'),
            'corCabelo' => $request->input('corCabelo'),
            'altura' => $request->input('altura'),
            'vestimentas' => $request->input('vestimentas'),
            'sexo' => $request->input('sexo'),
            'isDesaparecido' => $request->input('desaparecido') ? 1 : 0,
        ]);

        $foto = $request->file('foto');
        if ($foto) {
            $this->deleteImage($pessoa->foto);

            $caminho = $foto->storeAs('fotos', uniqid().'.'.$foto->extension());
            $pessoa->foto = $caminho;

            $this->removeFaceToCollection($pessoa->face_id);
            $faceId = $this->addFaceToCollection($caminho);
            $pessoa->face_id = $faceId;

        }

        $pessoa->save();
    }

    protected function deleteImage($caminho)
    {
        if (! $caminho) {
            return;
        }

        Storage::delete($caminho);
    }

    protected function removeFaceToCollection($faceId)
    {
        $client = $this->getAWSClient();

        $result = $client->DeleteFaces([
            'CollectionId' => 'AriadneFaces',
            'FaceIds' => [$faceId,],
        ]);
    }

    public function destroy(Pessoa $pessoa)
    {
        $this->deleteImage($pessoa->foto);
        $this->removeFaceToCollection($pessoa->face_id);
        $pessoa->locais()->delete();
        $pessoa->delete();
    }

    public function buscarByImage(UploadedFile $image)
    {
        $client = $this->getAWSClient();

        $results = $client->SearchFacesByImage([
            'CollectionId' => 'AriadneFaces',
            'Image' => [
                'Bytes' => $image->get(),
            ],
        ]);

        return $results->get('FaceMatches');
    }

    public function getByFaceIds(array $faceMatches)
    {
        $pessoas = [];
        foreach ($faceMatches as $face) {
            $faceId = $face['Face']['FaceId'];

            $pessoa = $this->getByFaceId($faceId);
            if (! $pessoa) {
                continue;
            }

            $pessoas[] = $pessoa;
        }

        return $pessoas;
    }

    public function getByFaceId(string $faceId)
    {
        return Pessoa::where('face_id', $faceId)->first();
    }
}