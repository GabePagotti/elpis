<?php
namespace App\Repositories;

use App\Http\Requests\LocalRequest;
use App\Models\Local;
use Illuminate\Support\Facades\Hash;

class LocalRepository
{
    public function find($id)
    {
        return Local::findOrFail($id);
    }

    public function store(LocalRequest $request)
    {
        return Local::create([
            'logradouro' => $request->input('logradouro'),
            'cidade' => $request->input('cidade'),
            'estado' => $request->input('estado'),
            'numero' => $request->input('numero'),
            'bairro' => $request->input('bairro'),
            'cep' => $request->input('cep'),
            'complemento' => $request->input('complemento'),
            'pessoa_id' => $request->input('pessoa_id'),
        ]);
    }

    public function destroy(Local $local)
    {
        $local->delete();
    }
}