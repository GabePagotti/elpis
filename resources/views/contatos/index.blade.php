<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.contatos') }}
        </h2>
    </x-slot>

    <x-card>
        <div class="float-right">
            @can('create', 'App\Models\Contato')<a href="{{ route('contatos.create') }}" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-default float-right mb-4">{{ __('web.cadastrar') }}</a>@endcan
        </div>
        <div class="table-responsive w-100">
            <x-table>
                <x-slot name="header">
                    <th>{{ __('#') }}</th>
                    <th>{{ __('web.nome') }}</th>
                    <th>{{ __('web.celular') }}</th>
                    <th>{{ __('web.parentesco') }}</th>
                    <th>{{ __('web.acoes') }}</th>
                </x-slot>
                <x-slot name="body">
                    @forelse($contatos as $contato)
                        <tr>
                            <td>{{ $contato->id }}</td>
                            <td>{{ $contato->nome }}</td>
                            <td>@celular($contato->celular)</td>
                            <td>{{ $contato->parentesco }}</td>
                            <td>
                                @can('view', $contato)<a href="{{ route('contatos.show', $contato->id) }}" class="link-secondary"><i class="fas fa-align-justify"></i></a>@endcan
                                @can('update', $contato)<a href="{{ route('contatos.edit', $contato->id) }}" class="link-primary"><i class="fas fa-pencil-alt"></i></a>@endcan
                                @can('delete', $contato)<a href="{{ route('contatos.destroy', $contato->id) }}" class="link-danger" onclick="return confirm('Tem certeza que deseja deletar esse contato?')"><i class="fas fa-trash-alt"></i></a>@endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">{{ __('web.sem_registros') }}</td>
                        </tr>
                    @endforelse
                </x-slot>
            </x-table>
        </div>
    </x-card>
</x-app-layout>
