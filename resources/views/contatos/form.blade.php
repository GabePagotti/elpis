<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.contatos') }}
        </h2>
    </x-slot>

    <x-card>
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="@isset($contato){{ route('contatos.update', $contato->id) }}@else{{ route('contatos.store') }}@endisset" enctype="multipart/form-data">
            @csrf
            @isset($contato)
                <x-input type="hidden" name="id" :value="$contato->id"/>
            @endisset


            <div class="row align-items-center">
                <div class="col-md-6">
                    <x-label for="nome" :value="__('web.nome')" />

                    <x-input id="nome" class="block mt-1 w-full" type="text" name="nome" :value="$contato->nome ?? old('nome')" required autofocus />
                </div>
                <div class="col-md-6">
                    <x-label for="parentesco">
                        {{ __('web.parentesco') }}
                    </x-label>

                    <x-input id="parentesco" class="block mt-1 w-full" type="text" name="parentesco" :value="$contato->parentesco ?? old('parentesco')" required />
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-4">
                    <x-label for="email" :value="__('web.email')" />

                    <x-input id="email" class="block mt-1 w-full" type="text" name="email" :value="isset($contato) ? $contato->email : old('email')" required />
                </div>

                <div class="col-md-4">
                    <x-label for="telefone">
                        {{ __('web.telefone') }} <small class="text-muted">Sem parênteses e hífens</small>
                    </x-label>

                    <x-input id="telefone" class="block mt-1 w-full" type="text" name="telefone" :value="$contato->telefone ?? old('telefone')" required />
                </div>

                <div class="col-md-4">
                    <x-label for="celular">
                        {{ __('web.celular') }} <small class="text-muted">Sem parênteses e hífens</small>
                    </x-label>

                    <x-input id="celular" class="block mt-1 w-full" type="text" name="celular" :value="$contato->celular ?? old('celular')" required />
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-6">
                    <x-label for="rg">
                        {{ __('web.rg') }} <small class="text-muted">Sem pontos e hífens</small>
                    </x-label>

                    <x-input id="rg" class="block mt-1 w-full" type="text" name="rg" :value="$contato->rg ?? old('rg')" required />
                </div>

                <div class="col-md-6">
                    <x-label for="cpf">
                        {{ __('web.cpf') }} <small class="text-muted">Sem pontos e hífens</small>
                    </x-label>

                    <x-input id="cpf" class="block mt-1 w-full" type="text" name="cpf" :value="$contato->cpf ?? old('cpf')" required />
                </div>
            </div>

            <div class="row mt-4">
            </div>

            <div class="mt-4 form-check">
                <x-input id="aceita_sms" class="form-check-input" type="checkbox" name="aceita_sms" :value="1" required checked />
                <x-label for="aceita_sms" class="form-check-label d-inline" :value="__('web.aceita_sms')" />
            </div>
            <div class="mt-4 form-check">
                <x-input id="aceita_email" class="form-check-input" type="checkbox" name="aceita_email" :value="1" required checked />
                <x-label for="aceita_email" class="form-check-label d-inline" :value="__('web.aceita_email')" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4 btn btn-outline-success">
                    {{ __('Salvar') }}
                </x-button>
            </div>
        </form>
    </x-card>
</x-app-layout>
