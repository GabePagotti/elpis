<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.users') }}
        </h2>
    </x-slot>

    <x-card>
        <div class="float-right">
            @can('create', 'App\Models\User')<a href="{{ route('users.create') }}" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-default float-right mb-4">{{ __('web.cadastrar') }}</a>@endcan
        </div>
        <div class="table-responsive w-100">
            <x-table>
                <x-slot name="header">
                    <th>{{ __('#') }}</th>
                    <th>{{ __('web.nome') }}</th>
                    <th>{{ __('web.email') }}</th>
                    <th>{{ __('web.permissao') }}</th>
                    <th>{{ __('web.acoes') }}</th>
                </x-slot>
                <x-slot name="body">
                    @forelse($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ __('web.'.$user->permissao) }}</td>
                            <td>
                                @can('view', $user)<a href="{{ route('users.show', $user->id) }}" class="link-secondary mr-1"><i class="fas fa-align-justify"></i></a>@endcan
                                @can('update', $user)<a href="{{ route('users.edit', $user->id) }}" class="link-primary mr-1"><i class="fas fa-pencil-alt"></i></a>@endcan
                                @can('delete', $user)<a href="{{ route('users.destroy', $user->id) }}" class="link-danger" onclick="return confirm('Tem certeza que deseja deletar esse usuário?')"><i class="fas fa-trash-alt"></i></a>@endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">{{ __('web.sem_registros') }}</td>
                        </tr>
                    @endforelse
                </x-slot>
            </x-table>
        </div>
    </x-card>
</x-app-layout>
