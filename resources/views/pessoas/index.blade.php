<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.pessoas') }}
        </h2>
    </x-slot>

    <x-card>
        <div class="float-right">
            <button class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-buscar mb-4 mr-1" data-bs-toggle="modal" data-bs-target="#busca">{{ __('web.buscar') }}</button>
            @can('create', 'App\Models\Pessoa')<a href="{{ route('pessoas.create') }}" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-default float-right mb-4">{{ __('web.cadastrar') }}</a>@endcan
        </div>
        <div class="table-responsive w-100">
            <x-table>
                <x-slot name="header">
                    <th>{{ __('#') }}</th>
                    <th>{{ __('web.nome') }}</th>
                    <th>{{ __('web.responsavel') }}</th>
                    <th>{{ __('web.dataNascimento') }} - {{ __('web.idade') }}</th>
                    <th>{{ __('web.sexo') }}</th>
                    <th>{{ __('web.acoes') }}</th>
                </x-slot>
                <x-slot name="body">
                    @forelse($pessoas as $pessoa)
                        <tr>
                            <td>{{ $pessoa->id }}</td>
                            <td>{{ $pessoa->nome }}</td>
                            <td>{{ $pessoa->user->name }}</td>
                            <td>{{ $pessoa->dataNascimento->format('d/m/Y') }} - @idade($pessoa->dataNascimento)</td>
                            <td>{{ __('web.sexo_'.$pessoa->sexo) }}</td>
                            <td>
                                @can('view', $pessoa)<a href="{{ route('pessoas.show', $pessoa->id) }}" class="link-secondary"><i class="fas fa-align-justify"></i></a>@endcan
                                @can('update', $pessoa)<a href="{{ route('pessoas.edit', $pessoa->id) }}" class="link-primary"><i class="fas fa-pencil-alt"></i></a>@endcan
                                @can('delete', $pessoa)<a href="{{ route('pessoas.destroy', $pessoa->id) }}" class="link-danger" onclick="return confirm('Tem certeza que deseja deletar esse usuário?')"><i class="fas fa-trash-alt"></i></a>@endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">{{ __('web.sem_registros') }}</td>
                        </tr>
                    @endforelse
                </x-slot>
            </x-table>
        </div>
    </x-card>

    <div class="modal fade" id="busca" tabindex="-1" aria-labelledby="buscaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="buscaLabel">{{ __('web.buscar') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" action="{{ route('pessoas.buscar') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <div class="mb-3">
                            <div>
                                <x-label for="foto" :value="__('web.foto')" />

                                <x-input id="foto" class="block mt-1 w-full" type="file" name="foto" required/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 btn-secondary" data-bs-dismiss="modal">{{ __('web.fechar') }}</button>
                        <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest disabled:opacity-25 transition ease-in-out duration-150 button-buscar">{{ __('web.buscar') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
