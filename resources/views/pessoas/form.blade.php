<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('web.pessoas') }}
        </h2>
    </x-slot>

    <x-card>
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="@isset($pessoa){{ route('pessoas.update', $pessoa->id) }}@else{{ route('pessoas.store') }}@endisset" enctype="multipart/form-data">
            @csrf
            @isset($pessoa)
                <x-input type="hidden" name="id" :value="$pessoa->id"/>
            @endisset


            <div class="row align-items-center">
                <div class="col-md-6">
                    <x-label for="nome" :value="__('web.nome')" />

                    <x-input id="nome" class="block mt-1 w-full" type="text" name="nome" :value="$pessoa->nome ?? old('nome')" required autofocus />
                </div>
                <div class="col-md-6">
                    <x-label for="foto" :value="__('web.foto')" />

                    <x-input id="foto" class="block mt-1 w-full" type="file" name="foto"/>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-4">
                    <x-label for="dataNascimento" :value="__('web.dataNascimento')" />

                    <x-input id="dataNascimento" class="block mt-1 w-full" type="date" name="dataNascimento" :value="isset($pessoa) ? $pessoa->dataNascimento->format('Y-m-d') : old('dataNascimento')" required />
                </div>

                <div class="col-md-4">
                    <x-label for="rg">
                        {{ __('web.rg') }} <small class="text-muted">Sem pontos e hífens</small>
                    </x-label>

                    <x-input id="rg" class="block mt-1 w-full" type="text" name="rg" :value="$pessoa->rg ?? old('rg')" required />
                </div>

                <div class="col-md-4">
                    <x-label for="cpf">
                        {{ __('web.cpf') }} <small class="text-muted">Sem pontos e hífens</small>
                    </x-label>

                    <x-input id="cpf" class="block mt-1 w-full" type="text" name="cpf" :value="$pessoa->cpf ?? old('cpf')" required />
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-3">
                    <x-label for="corPele" :value="__('web.corPele')" />

                    <x-input id="corPele" class="block mt-1 w-full" type="text" name="corPele" :value="$pessoa->corPele ?? old('corPele')" required />
                </div>

                <div class="col-md-3">
                    <x-label for="corOlho" :value="__('web.corOlho')" />

                    <x-input id="corOlho" class="block mt-1 w-full" type="text" name="corOlho" :value="$pessoa->corOlho ?? old('corOlho')" required />
                </div>

                <div class="col-md-3">
                    <x-label for="corCabelo" :value="__('web.corCabelo')" />

                    <x-input id="corCabelo" class="block mt-1 w-full" type="text" name="corCabelo" :value="$pessoa->corCabelo ?? old('corCabelo')" required />
                </div>
                <div class="col-md-3">
                    <x-label for="altura">
                        {{ __('web.altura') }} <small class="text-muted">Em centímetros</small>
                    </x-label>

                    <x-input id="altura" class="block mt-1 w-full" type="text" name="altura" :value="$pessoa->altura ?? old('altura')" required />
                </div>
            </div>

            <div class="mt-4">
                <x-label for="vestimentas" :value="__('web.vestimentas')" />

                <x-text-area id="vestimentas" class="block mt-1 w-full" name="vestimentas" :text="$pessoa->vestimentas ?? old('vestimentas')" required rows="4"/>
            </div>

            <div class="mt-4">
                <x-label :value="__('web.sexo')" />
                <div class="form-check form-check-inline">
                    <x-input id="sexo_M" class="form-check-input" type="radio" name="sexo" :value="'M'" required :checked="((isset($pessoa) && $pessoa->sexo == 'M') || old('sexo') == 'M') ? 'checked' : null"/>
                    <x-label for="sexo_M" class="d-inline form-check-label" :value="__('web.sexo_M')" />
                </div>

                <div class="form-check form-check-inline">
                    <x-input id="sexo_F" class="form-check-input" type="radio" name="sexo" :value="'F'" required :checked="((isset($pessoa) && $pessoa->sexo == 'F') || old('sexo') == 'F') ? 'checked' : null"/>
                    <x-label for="sexo_F" class="d-inline form-check-label" :value="__('web.sexo_F')" />
                </div>
            </div>

            <div class="mt-4 form-check">
                <x-input id="desaparecido" class="form-check-input" type="checkbox" name="desaparecido" :value="1" required checked />
                <x-label for="desaparecido" class="form-check-label d-inline" :value="__('web.desaparecido')" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4 btn btn-outline-success">
                    {{ __('Salvar') }}
                </x-button>
            </div>
        </form>
    </x-card>
</x-app-layout>
