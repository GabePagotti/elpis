<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePessoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('isDesaparecido');
            $table->string('hashFacial')->nullable();
            $table->string('dataNascimento');
            $table->enum('sexo', ['F', 'M']);
            $table->string('rg');
            $table->string('cpf');
            $table->string('corPele');
            $table->string('corOlho');
            $table->string('corCabelo');
            $table->integer('altura');
            $table->string('vestimentas');
            $table->string('foto')->nullable();
            $table->foreignId('user_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
